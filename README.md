# Hightlighting Doxygen Comments in VHDL 
## Features

![Syntax Highlighting](hightlights.png)

## Requirements

This extension was created as an extension to [rust_hdl_vscode](https://github.com/VHDL-LS/rust_hdl_vscode) and requires it or an equivalent extension to provide the base VHDL language support.

## Known Issues

Currently the hightlight color for Doxygen comments needs to be set manually in the swettings as follows:

    {
        "editor.tokenColorCustomizations": {
            "textMateRules": [
                {
                    "scope": "comment.doxygen",
                    "settings": {
                        "foreground": "#fca849"
                    }
                },
                {
                    "scope": "variable.doxygen",
                    "settings": {
                        "foreground": "#41d67f"
                    }
                }
            ]
        }
    }

## Todo

- [ ] add settings to set highlight color more elegantly

## Release Notes

### 0.1.0

Initial Release

### 0.2.0

Fix minor mistake in doxygen grammar

### 0.3.0

Add highlighting for doxygen variables